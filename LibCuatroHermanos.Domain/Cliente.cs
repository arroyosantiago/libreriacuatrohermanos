﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Cliente
    {
        [Required]
        public int ClienteId { get; set; }
        [Required]
        public int DomicilioId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(45)]
        public string Apellido { get; set; }
        [Required]
        [MaxLength(45)]
        public string DNI { get; set; }
        [Required]
        public DateTime FechaNacimiento { get; set; }
        [Required]
        [MaxLength(45)]
        public string Telefono { get; set; }
        [Required]
        [MaxLength(45)]
        public string Correo { get; set; }

        public Domicilio Domicilio { get; set; }
    }
}