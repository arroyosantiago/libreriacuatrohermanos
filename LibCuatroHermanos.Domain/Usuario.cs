﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Usuario
    {
        [Required]
        public int Id { get; set; }
        public int EmpleadoId { get; set; }
        public int RolUsuarioId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Username { get; set; }
        [Required]
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }

        public Empleado Empleado { get; set; }
        public RolUsuario RolUsuario { get; set; }
    }
}