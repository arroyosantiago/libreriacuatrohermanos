﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class LineaVenta
    {
        [Required]
        public int LineaVentaId { get; set; }
        [Required]
        public int VentaId { get; set; }
        [Required]
        public int ProductoId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Descripcion { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public decimal Subtotal { get; set; }

        public Venta Venta { get; set; }
        public Producto Producto { get; set; }
    }
}