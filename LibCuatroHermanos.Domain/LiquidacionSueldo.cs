﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibCuatroHermanos.Domain
{
    public class LiquidacionSueldo
    {
        [Required]
        public int LiquidacionSueldoId { get; set; }
        [Required]
        public int EmpleadoId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Empresa { get; set; }
        [Required]
        [MaxLength(45)]
        public string Domicilio { get; set; }
        [Required]
        [MaxLength(45)]
        public string Localidad { get; set; }
        [Required]
        [MaxLength(45)]
        public string Cuit { get; set; }
        [Required]
        [MaxLength(45)]
        public string Banco { get; set; }
        [Required]
        [MaxLength(45)]
        public string LocalidadBanco { get; set; }
        [Required]
        public DateTime FechaInicio { get; set; }
        [Required]
        public DateTime FechaPago { get; set; }
        [Required]
        public DateTime FechaDeposito { get; set; }
        [Required]
        public decimal Total { get; set; }

        public Empleado Empleado { get; set; }
    }
}
