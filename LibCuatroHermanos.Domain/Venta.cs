﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Venta
    {
        public Venta()
        {
            LineasVenta = new List<LineaVenta>();
        }
        [Required]
        public int VentaId { get; set; }
        [Required]
        public int ClienteId { get; set; }
        [Required]
        public int UsuarioId { get; set; }
        [Required]
        public DateTime FechaVenta { get; set; }
        [Required]
        public decimal Total { get; set; }

        public Cliente Cliente { get; set; }
        public Usuario Usuario { get; set; }
        public List<LineaVenta> LineasVenta { get; set; }
    }
}