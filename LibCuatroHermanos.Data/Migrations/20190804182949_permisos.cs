﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LibCuatroHermanos.Data.Migrations
{
    public partial class permisos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Permisos");

            migrationBuilder.CreateTable(
                name: "PermisosRolUsuarios",
                columns: table => new
                {
                    PermisoRolUsuarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CaracteristicaId = table.Column<int>(nullable: false),
                    RolUsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermisosRolUsuarios", x => x.PermisoRolUsuarioId);
                    table.ForeignKey(
                        name: "FK_PermisosRolUsuarios_Caracteristicas_CaracteristicaId",
                        column: x => x.CaracteristicaId,
                        principalTable: "Caracteristicas",
                        principalColumn: "CaracteristicaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermisosRolUsuarios_RolUsuarios_RolUsuarioId",
                        column: x => x.RolUsuarioId,
                        principalTable: "RolUsuarios",
                        principalColumn: "RolUsuarioId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PermisosRolUsuarios_CaracteristicaId",
                table: "PermisosRolUsuarios",
                column: "CaracteristicaId");

            migrationBuilder.CreateIndex(
                name: "IX_PermisosRolUsuarios_RolUsuarioId",
                table: "PermisosRolUsuarios",
                column: "RolUsuarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PermisosRolUsuarios");

            migrationBuilder.CreateTable(
                name: "Permisos",
                columns: table => new
                {
                    PermisoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CaracteristicaId = table.Column<int>(nullable: false),
                    RolUsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permisos", x => x.PermisoId);
                    table.ForeignKey(
                        name: "FK_Permisos_Caracteristicas_CaracteristicaId",
                        column: x => x.CaracteristicaId,
                        principalTable: "Caracteristicas",
                        principalColumn: "CaracteristicaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Permisos_CaracteristicaId",
                table: "Permisos",
                column: "CaracteristicaId");
        }
    }
}
